<?php

namespace Drupal\tattwa_clock\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\tattwa_clock\TattwaClockInterface;

/**
 * Provides the Tattwa Clock block.
 *
 * @Block(
 *   id = "tattwa_clock",
 *   admin_label = @Translation("Tattwa Clock"),
 *   category = @Translation("Tattwa Clock")
 * )
 */
class TattwaClock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The tattwa_clock service.
   *
   * @var \Drupal\tattwa_clock\TattwaClockInterface
   */
  protected $tattwaClock;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\tattwa_clock\TattwaClockInterface $tattwa_clock
   *   The tattwa_clock service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    TattwaClockInterface $tattwa_clock
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->tattwaClock = $tattwa_clock;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('tattwa_clock')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $sunrise = $this->tattwaClock->getSunriseTime();

    $build['tattwa_clock'] = [
      '#markup' => $this->t('Sunrise: @sunrise', ['@sunrise' => $sunrise]),
    ];

    return $build;
  }

}
