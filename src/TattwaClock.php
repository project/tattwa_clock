<?php

namespace Drupal\tattwa_clock;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class TattwaClock.
 */
class TattwaClock implements TattwaClockInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Current user from current session.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The tattwas.
   *
   * @var array
   */
  private $tattwas;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AccountInterface $current_user, TranslationInterface $string_translation) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
    $this->stringTranslation = $string_translation;

    $this->tattwas = [
      $this->t('Spirit'),
      $this->t('Air'),
      $this->t('Fire'),
      $this->t('Water'),
      $this->t('Earth'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getTattwas() {
    return $this->tattwas;
  }

  /**
   * {@inheritdoc}
   */
  public function getSunriseTime() {
    // Getting the current user entity.
    $user = $this->entityTypeManager->getStorage('user')->load($this->currentUser->id());

    // Getting the coordinates.
    $latitude = $user->get('field_location')->lat;
    $longitude = $user->get('field_location')->lng;
    // Calculating the zenith.
    $zenith = 90 + (50 / 60);

    // Getting the coordinate timezone.
    $coordinate_timezone = geotimezone_query([
      'latitude'  => $latitude,
      'longitude' => $longitude,
    ]);

    // Getting the actual time.
    $time = time();

    // Getting the timezone information.
    $date_for_timezone = DrupalDateTime::createFromTimestamp($time, $coordinate_timezone['identifier']);
    $gmt = $date_for_timezone->format("Z");
    // Converting the gmt minutes to the float number needed by the date_sunrise
    // function.
    $gmt_offset = $gmt / 60 / 60;

    // Getting the sunrise time.
    return date_sunrise($time, SUNFUNCS_RET_STRING, $latitude, $longitude, $zenith, $gmt_offset);
  }

}
