<?php

namespace Drupal\tattwa_clock;

/**
 * Interface TattwaClockInterface.
 */
interface TattwaClockInterface {

  /**
   * Returns the sunrise time.
   *
   * @return string
   *   The sunrise time.
   */
  public function getSunriseTime();

  /**
   * Returns the tattwas array.
   *
   * @return array
   *   The tattwas array.
   */
  public function getTattwas();

}
